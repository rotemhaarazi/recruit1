<?php

use Illuminate\Database\Seeder;

class UserinterviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Userinterviews')->insert([
            'name' => Str::random(10),
            'text' => Str::random(5).'@gmail.com',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            ]);  
        } 
}
