<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class InteviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            'name' => Str::random(10),
            'text' => Str::random(5),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);        
    }
}
