<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    protected $fillable = ['name','text','candidate'];

    public function candidate()
    {
        return $this->hasMany('App\Candidate');
    } 

}
