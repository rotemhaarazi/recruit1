<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User; 
use App\Department;
use App\Candidate;
use App\Status;
use App\Interview;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;



// full name is "App\Http\Controllers\CandidatesController"; 
class InterviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $interviews = Interview::all();      
        return view('interviews.index', compact('candidates','users', 'statuses','interviews'));
    }

     

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('interviews.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $interview = new Interview();
        //$candidate->name = $request->name; 
        //$candidate->email = $request->email;
        $can = $interview->create($request->all());
        // $can->status_id = 1;
        $can->save();
        return redirect('interviews');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $interview = Interview::findOrFail($id);
        return view('interviews.show', compact('interview'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Gate::authorize('add-user');
        $departments = Department::all();
        $interviews = Interview::findOrFail($id);
        return view('interviews.edit', compact('interview','departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //     Gate::authorize('add-user');
    //     $user = user::findOrFail($id);
    //    if(!isset($request->password)){
    //     $request['password'] = $user->password;   
    //    }else{
    //      $request->password = Hash::make($request['password']);   
    //    } 
    //    $user->update($request->all());
    //    return redirect('users');  
    // }

    public function myinterviews()
        {        
            $userId = Auth::id();
            $user = User::findOrFail($userId);
            $candidates = $user->candidates;
            //$candidates = Candidate::all();
            $users = User::all();
            $statuses = Status::all();        
            return view('candidates.index', compact('users', 'statuses'));
        }
    
        

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $interview = Interview::findOrFail($id);
        $interview->delete(); 
        return redirect('interviews'); 
    }
}