<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::post('candidates/changestatus/', 'CandidatesController@changeStatusFromCadidate')->name('candidates.changestatusfromcandidate')->middleware('auth');

Route::resource('candidates', 'CandidatesController')->middleware('auth');
Route::resource('interviews', 'InterviewsController')->middleware('auth');

Route::get('candidates/changeuser/{cid}/{uid?}', 'CandidatesController@changeUser')->name('candidate.changeuser');
Route::get('candidates/changestatus/{cid}/{sid}', 'CandidatesController@changeStatus')->name('candidates.changestatus')->middleware('auth');

Route::get('mycandidates', 'CandidatesController@myCandidates')->name('candidates.mycandidates')->middleware('auth');
Route::get('myinterviews', 'InterviewsController@myinterviews')->name('interviews.myinterviews')->middleware('auth');


Route::get('interviews/delete/{id}', 'CandidatesController@destroy')->name('interview.delete');

Route::get('candidates/delete/{id}', 'CandidatesController@destroy')->name('candidate.delete');

Route::get('/users/{email}/{name?}', function ($email,$name = null) {
    if(!isset($name)){
        $name = 'Mising name';
    }
    return view('users', compact('email','name'));
});




#example
Route::get('/users1/{name?}/{email}', function ($name = null, $email) {
    if(!isset($name)){
        $name = 'Mising name';
    }
    return view('users', compact('email','name'));
});























Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
