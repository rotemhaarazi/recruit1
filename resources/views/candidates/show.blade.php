@extends('layouts.app')

@section('title', 'Interview')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<h1>Interview details</h1>
<table class = "table table-dark">
    <!-- the table data -->
        <tr>
            <td>Id</td><td>
            <a href = "{{route('interviews.show',$interview->id)}}">{{$interview->name}}</a></td>
        </tr>
        <tr>
            <td>Name</td><td>{{$interview->name}}</td>
        </tr>
        <tr>
            <td>Email</td><td>{{$interview->text}}</td>
        </tr> 
        
@endsection
