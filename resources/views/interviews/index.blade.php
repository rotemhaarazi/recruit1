@extends('layouts.app')

@section('title', 'Interview')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<div><a href =  "{{url('/interviews/create')}}"> Add new interview</a></div>
<h1>List of interviews</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Name</th><th>Text</th><th>Owner</th><th>Created</th><th>Updated</th>
    </tr>
    <!-- the table data -->
    @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->id}}</td>
            <td>{{$interview->name}}</td>
            <td>{{$interview->text}}</td> 
            <td>{{$interview->candidate}}</td> 

            <td>{{$interview->created_at}}</td>
            <td>{{$interview->updated_at}}</td>
            <td>
                                                     
        </tr>
    @endforeach
</table>
@endsection


               
