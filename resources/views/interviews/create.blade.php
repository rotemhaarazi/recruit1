@extends('layouts.app')

@section('title', 'Create interview')

@section('content')
        <h1>Create interview</h1>
        <form method = "post" action = "{{action('InterviewsController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "name">interview name</label>
            <input type = "text" class="form-control" name = "name">
        </div>     
        <div class="form-group">
            <label for = "email">interview text</label>
            <input type = "text" class="form-control" name = "text">
        </div> 
        <div class="form-group">
            <div class="dropdown">
            <label for = "candidate">choose candidate</label>
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if(isset($interview->candidate_id))
                                    {{$interview->$candidate->name}}  
                                    @else
                                    Assign candidate
                                    @endif
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            </div>                
            <input type = "text" class="form-control" name = "text">
        </div> 
        <div>
            <input type = "submit" name = "submit" value = "Create interview">
        </div>                       
        </form>    
@endsection
